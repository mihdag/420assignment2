from person import Person

class Student(Person):
    def __init__(self, first_name, last_name, age, grade):
        super().__init__(first_name, last_name, age)
        if not 0 <= float(grade) <= 100:
            raise ValueError("Grade must be a number between 0 and 100")
        self.grade = grade

    def map_grade(self):
        if float(self.grade) >= 90:
            return 'A'
        elif float(self.grade) >= 80:
            return 'B'
        elif float(self.grade) >= 70:
            return 'C'
        elif float(self.grade) >= 60:
            return 'D'
        else:
            return 'F'

    def __str__(self):
        return self.first_name + " " + self.last_name + ", " + self.age + ", " + self.map_grade()