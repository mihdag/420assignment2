class Address:
    def __init__(self, name, street, city, province):
        if not isinstance(name, str) or not isinstance(street, str) or not isinstance(city, str) or not isinstance(province, str):
            raise ValueError("All parameters must be strings")

        self.name = name
        self.street = street
        self.city = city
        self.province = province

    def __str__(self):
        return self.name + ": " + self.street + ", " + self.city + ", " + self.province