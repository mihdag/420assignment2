from flask import Flask, request, render_template

from address import Address
from person import Person
from student import Student

app = Flask(__name__)

addresses = []
people = []

@app.route("/")
def home():
    return render_template('home.html')

@app.route("/newaddress")
def new_address():
    return render_template('newaddress.html')

@app.route("/listaddresses")
def list_addresses():
    html = "<ul>"
    for address in addresses:
        html += "<li>" + str(address) + "</li>"
    html += "</ul>"
    return html

@app.route("/newaddress", methods=['POST'])
def new_address_post():
    name = request.form['name']
    street = request.form['street']
    city = request.form['city']
    province = request.form['province']
    new_address = Address(name, street, city, province)
    addresses.append(new_address)
    return str(new_address)

@app.route("/listpeople")
def list_people():
    html = "<ul>"
    for person in people:
        if isinstance(person, Student):
            person_type = "Student"
        else:
            person_type = "Person"
        html += "<li>" + person_type + ": " + str(person) + "</li>"
    html += "</ul>"
    return html

@app.route("/newperson")
def new_person():
    return render_template('newperson.html')

@app.route("/newperson", methods=['POST'])
def new_person_post():
    first_name = request.form['first_name']
    last_name = request.form['last_name']
    age = request.form['age']
    new_person = Person(first_name, last_name, age)
    people.append(new_person)
    return str(new_person)

@app.route("/newstudent")
def newstudent():
    return render_template('newstudent.html')

@app.route("/newstudent", methods=['POST'])
def newstudent_post():
    first_name = request.form['first_name']
    last_name = request.form['last_name']
    age = request.form['age']
    grade = request.form['grade']
    new_student = Student(first_name, last_name, age, grade)
    people.append(new_student)
    return str(new_student)

