class Person:
    def __init__(self, first_name, last_name, age):
        if not isinstance(first_name, str):
            raise TypeError("First name must be a string")
        if not isinstance(last_name, str):
            raise TypeError("Last name must be a string")
        if not isinstance(int(age), int):
            raise TypeError("Age must be an integer")
        if int(age) < 0:
            raise ValueError("Age must be a positive integer")

        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def __str__(self):
        return self.first_name + " " + self.last_name + ", " + self.age